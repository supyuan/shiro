#shiro
基于shiro身份验证和授权集成小框架源代码
核心配置文件如下
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:util="http://www.springframework.org/schema/util"
	   xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
	   http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-3.2.xsd"
	default-lazy-init="true">
	
	<!--shiro主过滤器的配置，这里的名字和web中的要对应  -->  
	<bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
	<!-- 集成上安全管理器 -->  
		<property name="securityManager" ref="securityManager" />
		<!-- 全局默认的未登录跳转 验证路径 -->
		<property name="loginUrl" value="/login.jhtml" />
		<property name="successUrl" value="/" />
		<property name="filters">
			<util:map>
				<!-- 基于表单认证的过滤器 -->
				<entry key="authc" value-ref="authcFilter" />
				<!-- shiro内置过滤器 表示必须存在用户，当登入操作时不做检查 -->
				<entry key="user" value-ref="userFilter" />
				<!-- shiro内置过滤器 用户退出 -->
				<entry key="logout" value-ref="logoutFilter" />
			</util:map>
		</property>
		<!-- 过滤器链，对URL配置过滤规则 -->  
		<!--anon匿名 authc登录认证  user用户已登录 logout退出filter-->
		<property name="filterChainDefinitions">
			<value>
				*.jsp = anon
				/login.jhtml = authc
				/logout.jhtml = logout
				/admin/login.do = authc
				/admin/logout.do = logout
				/*.htm = user
				/*/**.htm = user
				/*/*/**.htm = user
			</value>
		</property>
	</bean>
	<!-- Shiro Filter -->	
	<bean id="adminUrlBean" class="org.core.security.CmsAdminUrl">
	<!-- 自定义字段 登录地址  用在CmsAuthenticationFilter 158行 判断 是不是请求的一个登录地址 -->
		<property name="adminLogin" value="/admin/login.do"/>
		<!-- 自定字段  用在 CmsAuthenticationFilter 72行 判断是 不是后台管理的请求-->
		<property name="adminPrefix" value="/admin/"/>
	</bean>
	<bean id="authcFilter" class="org.core.security.CmsAuthenticationFilter" parent="adminUrlBean">
		<!-- 自定义字段   用在 CmsAuthenticationFilter 142行  替换默认的 successUrl 认证后的url跳转 -->
		<property name="adminIndex" value="/admin/index.do"/>
		<property name="adminIndexI" value="/"/>
		<property name="adminIndexII" value="/jeeadmin/jeecms/"/>
	</bean>
	<bean id="userFilter" class="org.core.security.CmsUserFilter" parent="adminUrlBean"/>
	<bean id="logoutFilter" class="org.core.security.CmsLogoutFilter" parent="adminUrlBean"/>
	<bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
		<property name="realm" ref="authorizingRealm" />
		<property name="cacheManager" ref="shiroEhcacheManager"/>
	</bean>
	<bean id="authorizingRealm" class="org.core.security.CmsAuthorizingRealm">
		<property name="credentialsMatcher">
           <bean class="org.apache.shiro.authc.credential.HashedCredentialsMatcher">
               <property name="hashAlgorithmName" value="MD5"/>
             <!--   true means hex encoded, false means base64 encoded -->
               <property name="storedCredentialsHexEncoded" value="true"/>
               <!-- 迭代次数 -->
               <property name="hashIterations" value="1" />
           </bean>
        </property> 
		<!-- <property name="userSvc" ref="userSvc" /> -->
	</bean>
	<bean id="shiroEhcacheManager" class="org.apache.shiro.cache.ehcache.EhCacheManager">
		<property name="cacheManagerConfigFile">
				<value>classpath:config/ehcache/ehcache-shiro.xml</value>
		</property>
	</bean>
	
	<!-- Enable Shiro Annotations for Spring-configured beans.  Only run after -->
	<!-- the lifecycleBeanProcessor has run: -->
	<bean id="lifecycleBeanPostProcessor" class="org.apache.shiro.spring.LifecycleBeanPostProcessor" />
</beans>

数据库脚本 mysql

/*
Navicat MySQL Data Transfer

Source Server         : msql
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-04-20 16:00:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dept`
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `uids` int(32) NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `oder_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`uids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('10', '研发部', '0');
INSERT INTO `dept` VALUES ('11', '测试部', '1');
INSERT INTO `dept` VALUES ('12', '开发部', '2');
INSERT INTO `dept` VALUES ('13', '商务部', '3');

-- ----------------------------
-- Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `uids` int(32) NOT NULL,
  `names` varchar(50) NOT NULL,
  `age` int(32) NOT NULL,
  `dept_id` int(32) DEFAULT NULL,
  PRIMARY KEY (`uids`),
  KEY `fk_dept_id` (`dept_id`),
  CONSTRAINT `fk_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`uids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('-1923984386', '王五', '21', '11');
INSERT INTO `employee` VALUES ('-484553177', '1212', '21212', '13');
INSERT INTO `employee` VALUES ('-164300121', 'test', '12', '11');
INSERT INTO `employee` VALUES ('-81486416', 'ww', '12', '13');
INSERT INTO `employee` VALUES ('276124578', 'å°å', '12', '10');
INSERT INTO `employee` VALUES ('421236478', '1234', '21', '11');
INSERT INTO `employee` VALUES ('980418145', '小名', '21', '11');
INSERT INTO `employee` VALUES ('1426081620', '11', '1', '13');

-- ----------------------------
-- Table structure for `role_perms_tab`
-- ----------------------------
DROP TABLE IF EXISTS `role_perms_tab`;
CREATE TABLE `role_perms_tab` (
  `role_id` int(11) NOT NULL,
  `uri` varchar(100) NOT NULL,
  KEY `fk_jc_permission_role` (`role_id`),
  CONSTRAINT `fk_jc_permission_role` FOREIGN KEY (`role_id`) REFERENCES `role_tab` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色授权表';

-- ----------------------------
-- Records of role_perms_tab
-- ----------------------------
INSERT INTO `role_perms_tab` VALUES ('1', '/getMenuTree.do');
INSERT INTO `role_perms_tab` VALUES ('2', '/getMenuTree.do');
INSERT INTO `role_perms_tab` VALUES ('1', '@root');
INSERT INTO `role_perms_tab` VALUES ('2', '@root');

-- ----------------------------
-- Table structure for `role_tab`
-- ----------------------------
DROP TABLE IF EXISTS `role_tab`;
CREATE TABLE `role_tab` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `priority` int(11) NOT NULL DEFAULT '10' COMMENT '排列顺序',
  `is_super` char(1) NOT NULL DEFAULT '0' COMMENT '拥有所有权限',
  `role_code` varchar(255) NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of role_tab
-- ----------------------------
INSERT INTO `role_tab` VALUES ('1', '超级管理员', '10', '1', 'admin');
INSERT INTO `role_tab` VALUES ('2', '系统管理', '10', '0', 'system');

-- ----------------------------
-- Table structure for `s_menu`
-- ----------------------------
DROP TABLE IF EXISTS `s_menu`;
CREATE TABLE `s_menu` (
  `uids` varchar(32) NOT NULL COMMENT '主键',
  `realname` varchar(255) NOT NULL COMMENT '菜单名称',
  `qx_object` varchar(255) DEFAULT NULL COMMENT '权限对象 url 或者 权限字符串',
  `modelflag` varchar(32) NOT NULL COMMENT '节点类型（1功能模块 0功能操作）',
  `state` int(4) NOT NULL DEFAULT '0' COMMENT '状态 0正常 1 禁用 ',
  `parent_uids` varchar(32) NOT NULL COMMENT '父节点uids',
  `order_num` int(11) NOT NULL DEFAULT '0' COMMENT '排序号',
  PRIMARY KEY (`uids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_menu
-- ----------------------------
INSERT INTO `s_menu` VALUES ('01', '根节点', '@root', '1', '0', 'root', '0');
INSERT INTO `s_menu` VALUES ('02', '子节点', '/getMenuTree.do', '1', '0', '01', '1');
INSERT INTO `s_menu` VALUES ('03', '子节点', null, '1', '0', '02', '2');
INSERT INTO `s_menu` VALUES ('04', '测试1', null, '1', '0', '01', '3');
INSERT INTO `s_menu` VALUES ('05', '测试2', null, '1', '0', '04', '0');

-- ----------------------------
-- Table structure for `s_property`
-- ----------------------------
DROP TABLE IF EXISTS `s_property`;
CREATE TABLE `s_property` (
  `uids` varchar(32) NOT NULL,
  `property_code` varchar(255) NOT NULL COMMENT '属性编码 汉字除外 ',
  `property_value` varchar(255) NOT NULL COMMENT '属性值',
  `order_num` int(11) DEFAULT '0' COMMENT '排序号',
  PRIMARY KEY (`uids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of s_property
-- ----------------------------

-- ----------------------------
-- Table structure for `user_role_tab`
-- ----------------------------
DROP TABLE IF EXISTS `user_role_tab`;
CREATE TABLE `user_role_tab` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `fk_jc_role_user` (`user_id`),
  CONSTRAINT `fk_jc_role_user` FOREIGN KEY (`user_id`) REFERENCES `user_tab` (`user_id`),
  CONSTRAINT `fk_jc_user_role` FOREIGN KEY (`role_id`) REFERENCES `role_tab` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';

-- ----------------------------
-- Records of user_role_tab
-- ----------------------------
INSERT INTO `user_role_tab` VALUES ('1', '1');
INSERT INTO `user_role_tab` VALUES ('2', '1');
INSERT INTO `user_role_tab` VALUES ('1', '2');
INSERT INTO `user_role_tab` VALUES ('2', '2');
INSERT INTO `user_role_tab` VALUES ('1', '3');
INSERT INTO `user_role_tab` VALUES ('2', '3');

-- ----------------------------
-- Table structure for `user_tab`
-- ----------------------------
DROP TABLE IF EXISTS `user_tab`;
CREATE TABLE `user_tab` (
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `register_time` datetime NOT NULL COMMENT '注册时间',
  `register_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '注册IP',
  `last_login_ip` varchar(50) NOT NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否管理员',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `ak_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of user_tab
-- ----------------------------
INSERT INTO `user_tab` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, '2015-12-07 16:09:01', '127.0.0.1', '127.0.0.1', '0', '0', '0');
INSERT INTO `user_tab` VALUES ('2', 'test', 'e10adc3949ba59abbe56e057f20f883e', null, '2015-12-16 11:02:32', '127.0.0.1', '127.0.0.1', '0', '0', '0');
INSERT INTO `user_tab` VALUES ('3', 'test2', 'e10adc3949ba59abbe56e057f20f883e', null, '2015-12-16 11:03:08', '127.0.0.1', '127.0.0.1', '0', '0', '0');



